import Vue from 'vue'
import App from './App'
import store from './store'
import api from './common/vmeitime-http'
import conf from './common/conf'
import dayjs from 'dayjs';
import service from './common/api';
import util from './common/util';
import ClUni from "cl-uni";


Vue.prototype.$app = {
	
}
Vue.prototype.$eventHub = Vue.prototype.$eventHub || new Vue()
Vue.config.productionTip = false
Vue.prototype.$api=api;
Vue.prototype.$conf=conf;
Vue.prototype.$domain=conf.domain;
Vue.prototype.$store=store;
Vue.prototype.$dayjs=dayjs;
Vue.prototype.$service=service;
Vue.prototype.$util=util
Vue.prototype.$ImgRoot=conf.ImgUrl;

Vue.prototype.$fail=(msg)=>{
	uni.showToast({
		icon:'none',
		title:msg
	})
};

Vue.use(ClUni, {
	// 进入业务单页时，页面栈只有一个，自定义导航左侧返回按钮跳转的路径
	homePage: "/"
});

App.mpType = 'app'

const app = new Vue({
	store,
    ...App,
	created() {
		this.$store.dispatch('user/load');
	}
})
app.$mount()
