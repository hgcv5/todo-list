import {
	mapActions,
	mapState
} from "vuex"

export default {
	data() {
		return {

		}
	},
	created() {

	},
	onLoad() {

	},
	components: {

	},
	methods: {
		checkLogin() {
			return true;
		},
		toast(message, type = null) {
			if (this.$refs.toast) {
				this.$refs.toast.open({
					message,
					type
				});
			} else {
				uni.showToast({
					icon:'none',
					title:message
				})
			}
		},
			
		toPage(url){
			uni.navigateTo({
				url
			})
		}
	},
	computed: {
		...mapState("user", {
			isLogin: state => state.isLogin,
			user: state => state.user,
		})
	}

}
