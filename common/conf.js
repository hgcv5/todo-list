var dev_env = process.env.NODE_ENV === 'development' 

export default {
	appName:'九头鸟团长',
	isImgTitle:false,
	PROJECT_KEY:'baby',//关键前缀
	// domain: dev_env?'http://localhost:8035/syds_ci':'https://api.xiaoerbang.cn/syds_ci',
	domain: dev_env ? 'https://jtn.xiaoerbang.cn/syds_ci' : 'https://jtn.xiaoerbang.cn/syds_ci',
	authUrl: dev_env?'http://dsapp.g5kj.com/wechat.html':"http://ds.tiexiaoxia.com/wechat.html",
	AppId: dev_env?'wx581e802a9cf6dccf':"wx1f600407524b6de1",
	ImgUrl: dev_env?'http://nb.file.xiaoerbang.cn/':"http://jtn.file.xiaoerbang.cn/",//图片地址
}
// export default {
// 	PROJECT_KEY:'tyxs',//关键前缀
// 	domain: dev_env?'https://api.tyxs.xiaoerbang.cn/syds_ci':'https://api.tyxs.xiaoerbang.cn/syds_ci',
// 	authUrl: dev_env?'http://dsapp.g5kj.com/wechat.html':"http://ds.tiexiaoxia.com/wechat.html",
// 	AppId: dev_env?'wx581e802a9cf6dccf':"wx1f600407524b6de1",
// 	ImgUrl: dev_env?'http://ds.tiexiaoxia.com/':"http://img.tyxs.xiaoerbang.cn/",//图片地址
// }
