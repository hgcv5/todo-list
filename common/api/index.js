import http from "../vmeitime-http"
import conf from "../conf.js"

function requestByForm(url, data, method) {
	return new Promise((callback, error) => {
		uni.showLoading({
			title: '正在请求...'
		})
		data.customer_token=uni.getStorageSync(conf.PROJECT_KEY+'_token');
		uni.uploadFile({
			url: conf.domain + url,
			filePath: data.file,
			name: 'file',
			formData:data,
			success: (response) => {
				uni.hideLoading()
				if (response.statusCode === 200) {
					callback({ ...response
					})
				}
			}
		});
	})
}


/**
 * api 服务
 */
export default {
	//订单修改
	findHealthLog(data){
		return http.get('/app/health/listByApp',data,{loading:true});
	},
	/**
	 * 微信登录
	 * @param {Object} data
	 */
	loginUserByWechatCode(data){
		return http.post('/customer/customer/signCustomerByChatcode.action',{...data,customer_divicetype:'WXMP'},{loading:true});
	},
	//确认登记
	sureRegister(data) {
		return requestByForm('/commaderOutStock/addOutStock.action', data, 'POST');
	} ,
	
	
	
	
}
