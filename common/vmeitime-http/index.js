import http from './interface'
import conf from '../conf'
import store from '@/store'

/**
 * 将业务所有接口统一起来便于维护
 * 如果项目很大可以将 url 独立成文件，接口分成不同的模块
 * 
 */

// 单独导出(测试接口) import {test} from '@/common/vmeitime-http/'
export const test = (data) => {
	/* http.config.baseUrl = "http://localhost:8080/api/"
	//设置请求前拦截器
	http.interceptor.request = (config) => {
		config.header = {
			"token": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
		}
	} */
	//设置请求结束后拦截器
	http.interceptor.response = (response) => {
		console.log('个性化response....')
		//判断返回状态 执行相应操作
		return response;
	}
    return http.request({
		baseUrl: 'https://unidemo.dcloud.net.cn/',
        url: 'ajax/echo/text?name=uni-app',
		dataType: 'text',
        data,
    })
}


function getToken(){
	let token=  uni.getStorageSync(conf.PROJECT_KEY+'_token')
	//console.log(token)
	return token;
	//return 'Bearer ' + conf.token
}

// 默认全部导出  import api from '@/common/vmeitime-http/'
export default {
	...http,
	config:{
		...http.config,
		baseUrl: conf.domain,
		header:{
			...http.config.header
			
		}
	},
	interceptor:{
		...http.interceptor,
		request:(config)=>{
			config.header.Authorization = getToken();
			if(config.loading){
				uni.showLoading({
					title:config.loadMsg?config.loadMsg:'网络加载中..'
				})
			}
			//console.log("测试配置"+config);
		},
		response: (response)=>{
			uni.hideLoading();
			console.log(response.data)
			if(response.statusCode === 200 && response.data.code === 4023){
				//uni.setStorageSync('backUrl',uni.());
				
				var pages=getCurrentPages();
				var page=pages[pages.length-1];
				uni.setStorageSync('backUrl',page.route);
				window.location.href=response.data.url
			}else if(response.data &&  response.data.code === -1){
				store.dispatch('user/logOut')
			}else if(response.statusCode === 200 ){
				return {...response.data}
			}else{
				//throw new Error("请求错误")
			}
			return response
		}
	}
}