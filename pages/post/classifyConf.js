

export default [
	{
		id:'daily',
		name:"日常",
		icon:'/static/tag/qingdan.png',
		color:'#42B7E5',
		info:'家庭琐事?记事本子?位置摆放位置记录生活中的琐事内容',
	},
	{
		id:'partner',
		name:"伴侣",
		icon:'/static/tag/heart.png',
		color:'#ff0000',
		info:'伴侣间的共同事项？伴侣的喜爱？伴侣间的重要日期',
	},
	{
		id:'baby',
		name:"孩子",
		icon:'/static/tag/haizi.png',
		color:'#F8B62D',
		info:'为孩子建立的清单，教育素材?孩子激励成长?孩子的愿望?孩子的大事记录',
	},
	{
		id:'buy',
		name:"购物",
		icon:'/static/tag/buy.png',
		color:'#8BC03C',
		info:'生活用品采购?装修采购?孩子的愿望?孩子的大事记录',
	},
	{
		id:'investment',
		name:"投资",
		icon:'/static/tag/touzi.png',
		color:'#F8B62D',
		info:'理财投资?个人能力投资?小投资大收获,让你学习进度每一天',
	},
	
	
]