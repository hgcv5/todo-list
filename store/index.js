import Vue from 'vue'
import Vuex from 'vuex'
import user from './module/user.js'

import createPersistedState from "vuex-persistedstate"


Vue.use(Vuex)

const store = new Vuex.Store({
	modules: {
		user
	},
	plugins: [createPersistedState({
		storage: {
			getItem: key => uni.getStorageSync(key),
			setItem: (key, value) => uni.setStorageSync(key,value),
			removeItem: key => uni.removeStorageSync(key)
		},
		reducer(val) {
			//console.log("store", val)
			return {
				user: val.user
			}
		}
	})]
	
	
})

export default store
