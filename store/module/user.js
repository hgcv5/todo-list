import apiService from "../../common/api"
import util from "../../common/util"
import conf from "../../common/conf.js"
const DEFAULT_STATE = {
	isLogin: false,
	token: '',
	user: {},
	userAdd: {},
	role: 'commander',
	commander: {

	},
	wxuser: {

	},
	area_agent: {

	}
}
export default {
	namespaced: true,
	state: {
		...DEFAULT_STATE,
	},
	mutations: {
		userinfo(state, data) {
			state.isLogin = true;
			state.token = data.customer_token;
			state.user = data;

		},
		wxuser(state, data) {
			state.wxuser = data
		},
		init(state, data) {
			state.token = '';
			state.isLogin = false;
			state.user = {};
		},
		commander(state, data) {
			state.commander = data
		},
		roleUpdate(state, data) {
			state.role = data
		},
		area(state, data) {
			state.area_agent = data
		},
		logoutUser(state, data) {
			state.token = '';
			state.isLogin = false;
			state.user = {};
			state.area_agent = {}
			state.commander = {}
		}
	},
	actions: {
		 db({
			dispatch,
			commit,
			state
		}, meta) {
			return new Promise((back,error)=>{
				uni.setStorage({
					key: conf.PROJECT_KEY + '_token',
					data: meta.customer_token,
					success: function() {
						commit('userinfo', { ...meta
						});
						if (state.role === 'commander') {
							dispatch('queryCommander')
						} else {
							dispatch('queryAreaAgent')
						}
						back(true);
				
					}
				});
				uni.setStorage({
					key: conf.PROJECT_KEY + '_user',
					data: meta,
					success: function() {
				      
					}
				});
			})
			
			
			

		},
		//更新用户信息
		async queryUserInfo({
			commit,
			state
		}) {
			let {
				code,
				data
			} = await apiService.findCustomer({});
			if (code === 1 && data) {
				let user = { ...state.user,
					...data
				};
				commit('userinfo', user);
				//更新用户信息
				uni.setStorage({
					key: conf.PROJECT_KEY + '_user',
					data: user,
					success: function() {

					}
				});

			}
			return data;
		},
		async queryCommander({
			commit,
			state
		}) {
			let {
				code,
				datas = []
			} = await apiService.findCommander();
			if (datas && datas.length > 0) {
				commit('commander', datas[0]);
			}
			return new Promise((back, error) => {
				if (datas && datas.length > 0) {
					back(datas[0])
				} else {
					back(false)
				}
			
			})

		},
		async queryAreaAgent({
			commit,
			state
		}) {
			let {
				code,
				datas
			} = await apiService.findAreaAgent({
				agent_tel: state.user.customer_phone
			});
			if (datas && datas.length > 0) {
				commit('area', datas[0]);
			}
			return new Promise((back, error) => {
				if (datas && datas.length > 0) {
					back(datas[0])
				} else {
					back(false)
				}
			})
		},


		load({
			commit,
			state,
			dispatch
		}) {
			console.log("...加载用户数据")

			let user = uni.getStorageSync(conf.PROJECT_KEY + '_user')
			if (user) {
				user = typeof(user) === 'string' ? JSON.parse(user) : user
				dispatch('db', user);
			}

		},
		async logOut({
			commit,
			state
		}) {
			try {
				commit('logoutUser')
				let res = await uni.clearStorageSync();
				//location.href=window.location.origin+window.location.pathname	
				//util.getWechatCode(location.href)
			} catch (e) {
				// error
			}

		}
	}
}
